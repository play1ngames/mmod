﻿using MMODNewUI.Pages.Lab4.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MMODNewUI.Pages
{
    /// <summary>
    /// Логика взаимодействия для Lab4Page.xaml
    /// </summary>
    public partial class Lab4Page : UserControl
    {
        public Lab4Page()
        {
            InitializeComponent();
            int N = 10000;
            QSchema qs = new QSchema(new LinearGenerator(0.1, 0.3),
                                    new LinearGenerator(0.8, 0.9),
                                    new LinearGenerator(0.8, 0.9),
                                    new LinearGenerator(0.8, 0.9),
                                    0.001, N, new int[] {1,1});
            int N1 = 0;
            int N3 = 0;
            qs.Solver(ref N1, ref N3);
        }
    }
}
