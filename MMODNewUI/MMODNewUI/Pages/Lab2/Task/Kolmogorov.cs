﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMODNewUI.Pages.Lab2.Task
{
    public class Kolmogorov
    {
        private IList<decimal> _values;

        public Kolmogorov(IList<decimal> values)
        {
            this._values = values;
        }
        private decimal Fexp(decimal X,decimal lambda)
        {
            if (X < 0)
                return 0;
            return (decimal)(1 - Math.Exp((double)(-lambda * X)));
        }     

        public bool CheckExpCriteria(decimal lambda, double a = 0.05)
        {

            var tempValues = _values.ToList();
            tempValues.Sort();

            var F = new List<Tuple<decimal, decimal>>();
            decimal maxValue = 0;
            
            foreach (var val in tempValues)
            {
                F.Add(new Tuple<decimal, decimal>(val, (decimal)tempValues.Where(x => x <= val).Count() / (decimal)tempValues.Count));

                var temp1 = F.Last().Item2;
                var temp2 = Fexp(F.Last().Item1, lambda);
                var curValue = Math.Abs(temp1 - temp2);
                if (curValue > maxValue)
                    maxValue = curValue;
            }

            var CriteriaValue = (decimal)Math.Sqrt(tempValues.Count) * maxValue;

            var y = 1 - a;
            double TableCriteriaValue = 0.0;

            for (int i = 0; i  < Table.Count; i++)
            {
                if(Table[i].Item2 == y)
                {
                    TableCriteriaValue = Table[i].Item1;
                    break;
                }
                if(Table[i].Item2 > y)
                {
                    TableCriteriaValue = (Table[i].Item1 + Table[i - 1].Item1) / 2;
                    break;
                }

            }

            if (CriteriaValue > (decimal)TableCriteriaValue)
                return false;
            else return true;            
        }

        public List<Tuple<double, double>> Table
            = new List<Tuple<double, double>>()
        {
            new Tuple<double, double>(0.50, 0.0361),
            new Tuple<double, double>(0.54, 0.0675),
            new Tuple<double, double>(0.58, 0.1632),
            new Tuple<double, double>(0.62, 0.2236),
            new Tuple<double, double>(0.66, 0.2888),
            new Tuple<double, double>(0.70, 0.3560),
            new Tuple<double, double>(0.74, 0.4230),
            new Tuple<double, double>(0.78, 0.4880),
            new Tuple<double, double>(0.82, 0.5497),
            new Tuple<double, double>(0.86, 0.6073),
            new Tuple<double, double>(0.90, 0.6601),
            new Tuple<double, double>(0.94, 0.7079),
            new Tuple<double, double>(0.98, 0.7500),
            new Tuple<double, double>(1.02, 0.7889),
            new Tuple<double, double>(1.06, 0.8223),
            new Tuple<double, double>(1.10, 0.8514),
            new Tuple<double, double>(1.14, 0.8765),
            new Tuple<double, double>(1.18, 0.8981),
            new Tuple<double, double>(1.22, 0.9164),
            new Tuple<double, double>(1.26, 0.9319),
            new Tuple<double, double>(1.30, 0.9449),
            new Tuple<double, double>(1.34, 0.9557),
            new Tuple<double, double>(1.38, 0.9646),
            new Tuple<double, double>(1.42, 0.9718),
            new Tuple<double, double>(1.46, 0.9778),
            new Tuple<double, double>(1.50, 0.9826),
            new Tuple<double, double>(1.54, 0.9864),
            new Tuple<double, double>(1.58, 0.9895),
            new Tuple<double, double>(1.62, 0.9918),
            new Tuple<double, double>(1.66, 0.9938),
            new Tuple<double, double>(1.70, 0.9953),
            new Tuple<double, double>(1.74, 0.9973),
            new Tuple<double, double>(1.78, 0.9980),
            new Tuple<double, double>(1.82, 0.9985),
            new Tuple<double, double>(1.86, 0.9989),
            new Tuple<double, double>(1.90, 0.9992),
            new Tuple<double, double>(1.94, 0.9995),
            new Tuple<double, double>(1.98, 0.9998)
        };
    }
}
