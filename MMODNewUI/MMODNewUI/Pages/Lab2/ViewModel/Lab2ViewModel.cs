﻿using FirstFloor.ModernUI.Presentation;
using MMODNewUI.Pages.Lab1.Task;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMODNewUI.Pages.Lab2.Task;

namespace MMODNewUI.Pages.Lab2.ViewModel
{
    //todo: много всего разного
    public class Lab2ViewModel : NotifyPropertyChanged
    {
        #region vars
        private double _a; //равномерное от
        public double A
        {
            get { return _a; }
            set
            {
                if (value < _b)
                {
                    _a = value;
                    OnPropertyChanged("A");
                    FirstGenerator();
                }
                //
            }
        }

        private double _b; //равномерное до
        public double B
        {
            get { return _b; }
            set
            {
                if (value > _a)
                {
                    _b = value;
                    OnPropertyChanged("B");
                    FirstGenerator();
                }

                //
            }
        }


        private double _lambda; // параметр экспоненциального
        public double Lambda
        {
            get { return _lambda; }
            set
            {
                if(value > 0)
                {
                    _lambda = value;
                    OnPropertyChanged("Lambda");
                    SecondGenerator();
                }

                //
            }
        }

        private int _n; //количество
        public int N
        {
            get { return _n; }
            set
            {
                if(value >= 100)
                {
                    _n = value;
                    OnPropertyChanged("N");
                    LNN = value;
                }
            }
                //
        }

        private int _ln; //количество
        public int LNN
        {
            get { return _ln; }
            private set
            {
                
                _ln = (int)(2* (int)Math.Round(Math.Log(value)));
                OnPropertyChanged("LNN");
                FirstGenerator();
                SecondGenerator();

            }
            //
        }

    

        //равномерное
        public ObservableCollection<Lab2Model> FirstRandomValue { get; private set; }

        //экспоненц
        public ObservableCollection<Lab2Model> SecondRandomValue { get; private set; }

        #endregion


        #region Generators

        //равномерное
        private void FirstGenerator()
        {
            var gen = new MultiplicativeCongruentMethod(45123967);
            FirstRandomValue = new ObservableCollection<Lab2Model>();
            List<decimal> nums = new List<decimal>();
            foreach (var number in gen.Sequence())
            {
                if (nums.Count - 1 == N)
                    break;
                nums.Add((decimal)A + (number * (decimal)(B - A)));
            }
            decimal step = (decimal)((B - A) / LNN);
            decimal step1 = (decimal)A;
            decimal step2 = (decimal)A + step;
            for (int i = 0; i < LNN; i++)
            {
                FirstRandomValue.Add(new Lab2Model { Category = i.ToString(), Number = nums.Where(val => val > step1 && val <= step2).Count() });
                step1 += step;
                step2 += step;
            }
           // AvgX1 = nums.Sum() / N;
            foreach (var interval in FirstRandomValue)
            {
                interval.Number /= ((decimal)nums.Count * step);
            }
            OnPropertyChanged("AvgX1");
            OnPropertyChanged("FirstRandomValue");
        }

        private bool _Kolmogorov;
        public string Kolmogorov
        {
            get
            {
                if (_Kolmogorov)
                    return "Гипотеза не отвергается :)";
                else
                    return "Гипотеза отвергается :(";
            }
        }

        private bool _momentMethod;

        public string MomentMethod
        {
            get
            {
                if (_momentMethod)
                    return "Точечная оценка математического ожидания +";
                else
                    return "Точечная оценка математического ожидания -";

            }
        }


        //экспоненциальное
        private void SecondGenerator()
        {
            var gen = new MultiplicativeCongruentMethod(12378945);

            SecondRandomValue = new ObservableCollection<Lab2Model>();
            for (int i = 0; i < LNN; i++)
            {
                SecondRandomValue.Add(new Lab2Model { Category = i.ToString() });
            }

            List<decimal> nums = new List<decimal>();
            foreach(var number in gen.Sequence())
            {
                if (nums.Count == N)
                    break;
                nums.Add((decimal)(-1 /Lambda * Math.Log(Convert.ToDouble(number))));
            }
            decimal step = (nums.Max()-nums.Min()) / LNN;
            //decimal step = (decimal)(double.MaxValue / LNN);
            decimal step1 = nums.Min();
            decimal step2 = step;
            for (int i = 0; i < LNN; i++)
            {
                SecondRandomValue.Add(new Lab2Model { Category = i.ToString(), Number = nums.Where(val=> val > step1 && val <= step2).Count()});
                step1 += step;
                step2 += step;
            }
            foreach(var interval in SecondRandomValue)
            {
                interval.Number /= ((decimal)nums.Count * step);
            }
            //AvgX2 = nums.Sum() / N;
            OnPropertyChanged("AvgX2");
            OnPropertyChanged("SecondRandomValue");
            var kol = new Kolmogorov(nums);
            _Kolmogorov = kol.CheckExpCriteria((decimal)Lambda);
            OnPropertyChanged("Kolmogorov");

            //if (AvgX2 - (decimal)(1 / Lambda) < (decimal)0.05)
            //    _momentMethod = true;
            //else
            //    _momentMethod = false;
            Mx = nums.Sum() / N;
            OnPropertyChanged("Mx");
            s = (decimal)Math.Sqrt(nums.Select(x => Math.Pow((double)(x - Mx), 2)).Sum() / (nums.Count - 1));
            OnPropertyChanged("s");
            Dx = s * s;
            var index = Array.IndexOf(TableStudent.Select(x => x.Item1).ToArray(), nums.Count);
            if(index == -1)
            {
                t = 0;
                Mx1 = 0;
                Mx2 = 0;
                Dx1 = 0;
                Dx2= 0;
            }
            else
            {
                t = (decimal)TableStudent[index].Item2;
                var interv = t * s / (decimal)Math.Sqrt(nums.Count);
                Mx1 = Mx - interv;
                Mx2 = Mx + interv;
                Dx1 = nums.Count * Dx / (decimal)TablePearson[index].Item1;
                Dx2 = nums.Count * Dx / (decimal)TablePearson[index].Item2;
            }
            OnPropertyChanged("t");
            OnPropertyChanged("Mx1");
            OnPropertyChanged("Mx2");
            OnPropertyChanged("Dx");
            OnPropertyChanged("Dx1");
            OnPropertyChanged("Dx2");
        }

        #endregion
        //n       0.05     0.01    0.001

        //100 	1,984 	 2,626 	 3,390
        //110 	1,982 	 2,621 	 3,381
        //120 	1,980 	 2,617 	 3,373
        //130 	1.978 	 2,614 	 3,367
        //140 	1.977 	 2,611 	 3,361
        //150 	1.976 	 2,609 	 3,357
        //200 	1.972 	 2,601 	 3,340
        //250 	1.969 	 2,596 	 3,330
        //300 	1.968 	 2,592 	 3,323
        //350 	1.967 	 2,590 	 3,319

       public List<Tuple<double, double>> TableStudent
            = new List<Tuple<double, double>>()
        {
            new Tuple<double, double>(100, 1.984),
            new Tuple<double, double>(110, 1.982),
            new Tuple<double, double>(120, 1.980),
            new Tuple<double, double>(130, 1.978),
            new Tuple<double, double>(140, 1.977),
            new Tuple<double, double>(150, 1.976),
            new Tuple<double, double>(200, 1.972),
            new Tuple<double, double>(250, 1.969),
            new Tuple<double, double>(300, 1.968),
            new Tuple<double, double>(350, 1.967),
        };

        public List<Tuple<double, double>> TablePearson
            = new List<Tuple<double, double>>()
        {
            new Tuple<double, double>(129.5611972, 74.22192747),
            new Tuple<double, double>(140.9165728   ,82.86705394),
            new Tuple<double, double>(152.2114027   ,91.5726419),
            new Tuple<double, double>(163.4531424,  100.3312555),
            new Tuple<double, double>(174.6478322   ,109.1368682),
            new Tuple<double, double>(185.800447    ,117.9845154),
            new Tuple<double, double>(241.0578955,  162.7279825),
            new Tuple<double, double>(295.6886282   ,208.0977981),
            new Tuple<double, double>(349.8744688   ,253.9123226),
            new Tuple<double, double>(403.7233305   ,300.0637215),
        };
        //129.5611972	74.22192747
        //140.9165728	,82.86705394
        //152.2114027	,91.5726419
        //163.4531424,	100.3312555
        //174.6478322	,109.1368682
        //185.800447	,117.9845154
        //241.0578955,	162.7279825
        //295.6886282	,208.0977981
        //349.8744688	,253.9123226
        //403.7233305	,300.0637215


        #region analys
        ////среднее Х1
        //public decimal AvgX1 { get; private set; }
        //public decimal MX1 { get { return (decimal)(A + B) / 2; } }
        ////среднее Х2
        //public decimal AvgX2 { get; private set; }
        //public decimal MX2 { get { return (decimal)(1/Lambda); } }

        //МО
        public decimal Mx { get; private set; }
        public decimal s { get; private set; }
        public decimal t { get; private set; }
        public decimal Mx1 { get; private set; }
        public decimal Mx2 { get; private set; }

        public decimal Dx { get; private set; }
        public decimal Dx1 { get; private set; }
        public decimal Dx2 { get; private set; }
        #endregion

        public Lab2ViewModel()
        {
            _a = 1;
            _b = 10;
            _lambda = 2;
            N = 100;

            // add the default themes
            this.themes.Add(new Link { DisplayName = "dark", Source = AppearanceManager.DarkThemeSource });
            this.themes.Add(new Link { DisplayName = "light", Source = AppearanceManager.LightThemeSource });
            AppearanceManager.Current.PropertyChanged += OnAppearanceManagerPropertyChanged;
            SyncTheme();
        }

        #region themes
        private LinkCollection themes = new LinkCollection();
        private Link selectedTheme;
        public Link SelectedTheme
        {
            get { return this.selectedTheme; }
            set
            {
                if (this.selectedTheme != value)
                {
                    this.selectedTheme = value;
                    OnPropertyChanged("SelectedTheme");
                    OnPropertyChanged("DarkLayout");
                    OnPropertyChanged("Foreground");
                    OnPropertyChanged("Background");
                    OnPropertyChanged("MainBackground");
                    OnPropertyChanged("MainForeground");
                    // and update the actual theme
                    AppearanceManager.Current.ThemeSource = value.Source;
                }
            }
        }

        public string Foreground
        {
            get
            {
                if (selectedTheme.DisplayName.Equals("dark"))
                {
                    return "#FFEEEEEE";
                }
                return "#FF666666";
            }
        }
        public string MainForeground
        {
            get
            {
                if (selectedTheme.DisplayName.Equals("dark"))
                {
                    return "#FFFFFFFF";
                }
                return "#FF666666";
            }
        }
        public string Background
        {
            get
            {
                if (selectedTheme.DisplayName.Equals("dark"))
                {
                    return "#FF333333";
                }
                return "#FFF9F9F9";
            }
        }
        public string MainBackground
        {
            get
            {
                if (selectedTheme.DisplayName.Equals("dark"))
                {
                    return "#FF000000";
                }
                return "#FFEFEFEF";
            }
        }


        private void OnAppearanceManagerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ThemeSource")
            {
                SyncTheme();
            }
        }

        private void SyncTheme()
        {
            // synchronizes the selected viewmodel theme with the actual theme used by the appearance manager.
            this.SelectedTheme = this.themes.FirstOrDefault(l => l.Source.Equals(AppearanceManager.Current.ThemeSource));
        }


        #endregion

    }

}
