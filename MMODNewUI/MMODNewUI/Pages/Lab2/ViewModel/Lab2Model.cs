﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMODNewUI.Pages.Lab2.ViewModel
{
    public class Lab2Model
    {
        private decimal _number;
        public decimal Number { get { return Decimal.Round(_number, 3); } set { _number = value; } }
        public string Category { get; set; }
    }
}
