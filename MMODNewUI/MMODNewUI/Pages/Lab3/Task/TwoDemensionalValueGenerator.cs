﻿using MMODNewUI.Pages.Lab1.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMODNewUI.Pages.Lab3.Task
{
    public class TwoDemensionalValueGenerator
    {
        private const double eps = 0.001;

        private readonly double[,] P;
        private readonly double[] x;
        private readonly double[] y;
        private double[] Px;
        private double[] Py;
        private double[] Fx;
        private double[] Fy;

        public double[] GetPx()
        {
            double[] res = new double[x.GetLength(0)];
            Array.Copy(Px, res, res.GetLength(0));
            return res;
        }

        public double[] GetPy()
        {
            double[] res = new double[y.GetLength(0)];
            Array.Copy(Py, res, res.GetLength(0));
            return res;
        }


        private double[,] Pyx;
        private double[,] Fyx;
            
        private MultiplicativeCongruentMethod bsv;

        public TwoDemensionalValueGenerator(double[,] P, double[] x, double[] y)
        {
            if (P.GetLength(0) != x.GetLength(0) ||
                P.GetLength(1) != y.GetLength(0))
                throw new ArgumentException("Размерность матриц не совпадает");
            var tempSum = 0.0;
            for (int i = 0; i < P.GetLength(0); i++)
                for (int j = 0; j < P.GetLength(1); j++)
                    tempSum += P[i, j];
            if (tempSum < 1 - eps)
                throw new ArgumentException("Сумма вероятностей не валидна");

            this.x = x;
            this.y = y;
            this.P = P;

            Px = new double[x.GetLength(0)];
            Py = new double[y.GetLength(0)];
            for(int i = 0; i < x.GetLength(0); i++)
                for(int j = 0; j < y.GetLength(0); j++)
                {
                    Px[i] += P[i, j];
                    Py[j] += P[i, j];
                }

            Fx = new double[x.GetLength(0)];
            Fx[0] = Px[0];
            for(int i = 1; i < x.GetLength(0); i++)
                Fx[i] = Fx[i - 1] + Px[i];
            //Fy = new double[y.GetLength(0)];
            //Fy[0] = Py[0];
            //for (int i = 1; i < y.GetLength(0); i++)
            //    Fy[i] = Fy[i - 1] + Py[i];

            long time = DateTime.Now.Ticks;
            int seed = (int)(time % (long)99999999);
            if (seed < 10000000)
                seed += 10000000;

            Pyx = new double[P.GetLength(0), P.GetLength(1)];
            Fyx = new double[P.GetLength(0), P.GetLength(1)];

            for (int i = 0; i < x.GetLength(0); i++)
                for (int j = 0; j < y.GetLength(0); j++)
                    Pyx[i, j] = P[i, j] / Px[i];
            for (int i = 0; i < x.GetLength(0); i++)
                Fyx[i, 0] = Pyx[i, 0];
            for (int j = 1; j < y.GetLength(0); j++)
                for (int i = 0; i < x.GetLength(0); i++)
                    Fyx[i, j] = Fyx[i, j-1] + Pyx[i, j];

            bsv = new MultiplicativeCongruentMethod(seed);
        }

        /// <summary>
        /// Генератор псевдослучайных чисел
        /// </summary>
        /// <returns>Последовательность системы св </returns>
        public IEnumerable<Tuple<double,double>> Sequence(int count = -1)
        {
            int curCount = 0;
            bool phase = true;
            int k = 0;
            foreach (var e in bsv.Sequence())
            {
                if (curCount == count)
                    break;
                if (phase)
                {
                    k = 0;
                    double e1 = (double)e;

                    for(int i = 0; i < this.Fx.GetLength(0); i++)
                        if (e1 < Fx[i])
                        {
                            k = i;
                            break;
                        }
                    phase = false;
                    continue;
                }
                int l = 0;
                double e2 = (double)e;
                for (int i = 0; i < this.y.GetLength(0); i++)
                    if (e2 <Fyx[k,i])
                    {
                        l = i;
                        break;
                    }
                phase = true;
                curCount++;
                yield return new Tuple<double, double>(x[k], y[l]);
            }
        }
    }
}
