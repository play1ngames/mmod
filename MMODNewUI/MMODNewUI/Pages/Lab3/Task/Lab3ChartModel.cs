﻿using System;

namespace MMODNewUI.Pages.Lab3.Task
{
    public class Lab3ChartModel
    {
        private double _number;
        public double Number { get { return Math.Round(_number, 4); } set { _number = value; } }
        public string Category { get; set; }
    }
}
