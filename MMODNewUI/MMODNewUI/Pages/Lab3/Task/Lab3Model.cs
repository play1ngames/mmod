﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMODNewUI.Pages.Lab3.Task
{
    [Serializable]
    public class Lab3Model
    {
        public List<double[]> P { get; set; }
        public double[] x { get; set; }
        public double[] y { get; set; }

        public Lab3Model() { }

        public Lab3Model(List<double[]> P, double[] x, double[] y)
        {
            this.P = P;
            this.x = x;
            this.y = y;
        }

        public double[,] GetP()
        {
            //todo: check length
            var res = new double[P.Count, P[0].GetLength(0)];
            for (int i = 0; i < P.Count; i++)
                for (int j = 0; j < P[i].GetLength(0); j++)
                    res[i, j] = P[i][j];
            return res;
        }

    }
}
