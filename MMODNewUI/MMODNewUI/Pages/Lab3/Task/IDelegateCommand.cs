﻿
using System.Windows.Input;

namespace MMODNewUI.Pages.Lab3.Task
{
    interface IDelegateCommand : ICommand
    {
        void RaiseCanExecuteChanged();
    }
}

