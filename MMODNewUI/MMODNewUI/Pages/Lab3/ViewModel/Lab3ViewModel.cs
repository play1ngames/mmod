﻿using FirstFloor.ModernUI.Presentation;
using Microsoft.Win32;
using MMODNewUI.Pages.Lab3.Task;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;

namespace MMODNewUI.Pages.Lab3.ViewModel
{
    internal class Lab3ViewModel: NotifyPropertyChanged
    {
        private double[,] _P;
        public double[,] P
        {
            get { return _P; }
            private set
            {
                _P = value;
                OnPropertyChanged("P");
            }
        }

        private double[] _x;
        public double[] x
        {
            get { return _x; }
            private set
            {
                _x = value;
                OnPropertyChanged("x");
            }
        }

        private double[] _y;
        public double[] y
        {
            get { return _y; }
            private set
            {
                _y = value;
                OnPropertyChanged("y");
            }
        }

        private string sourceFile;
        public string SourceFile
        {
            get { return sourceFile ?? "Файл не выбран"; }
            private set
            {
                if (ReadValues(value))
                {
                    sourceFile = value;
                    OnPropertyChanged("SourceFile");
                    Calculate();
                }
            }
        }

        private int _count;
        public int Count
        {
            get
            {
                return _count;
            }
            set
            {
                _count = value;
                OnPropertyChanged("Count");
                Calculate();
            }
        }



        public ObservableCollection<Lab3ChartModel> XRandomValue { get; private set; }
        public ObservableCollection<Lab3ChartModel> YRandomValue { get; private set; }

        public ObservableCollection<Lab3ChartModel> XTheoryValue { get; private set; }
        public ObservableCollection<Lab3ChartModel> YTheoryValue { get; private set; }

        private double mX;
        public double MX { get; private set; }
        private double mY;
        public double MY { get; private set; }

        private double dX;
        public double DX { get; private set; }
        private double dY;
        public double DY { get; private set; }

        private double r;
        public double R { get; private set; }

        private double mXt;
        public double MXt { get; private set; }
        private double mYt;
        public double MYt { get; private set; }

        private double dXt;
        public double DXt { get; private set; }
        private double dYt;
        public double DYt { get; private set; }

        private double rt;
        public double Rt { get; private set; }

        private void Calculate()
        {
            var generator = new TwoDemensionalValueGenerator(P, x, y);

            XRandomValue = new ObservableCollection<Lab3ChartModel>();
            YRandomValue = new ObservableCollection<Lab3ChartModel>();
            var nums = generator.Sequence(Count);
            var numsX = nums.Select(v => v.Item1).ToList();
            var numsY = nums.Select(v => v.Item2).ToList();

            for (int i = 0; i < x.GetLength(0); i++)
                XRandomValue.Add(new Lab3ChartModel { Category = x[i].ToString(), Number = numsX.Where(val => val == x[i]).Count() });
            foreach (var interval in XRandomValue)
                interval.Number /= numsX.Count;
            OnPropertyChanged("XRandomValue");

            for (int i = 0; i < y.GetLength(0); i++)
                YRandomValue.Add(new Lab3ChartModel { Category = y[i].ToString(), Number = numsY.Where(val => Math.Round(val, 4) == Math.Round(y[i], 4)).Count() });
            foreach (var interval in YRandomValue)
                interval.Number /= numsY.Count;
            OnPropertyChanged("YRandomValue");

            XTheoryValue = new ObservableCollection<Lab3ChartModel>();
            YTheoryValue = new ObservableCollection<Lab3ChartModel>();
            var PX = generator.GetPx();
            var PY = generator.GetPy();
            for (int i = 0; i < x.GetLength(0); i++)
                XTheoryValue.Add(new Lab3ChartModel { Category = x[i].ToString(), Number = PX[i] });
            for (int i = 0; i < y.GetLength(0); i++)
                YTheoryValue.Add(new Lab3ChartModel { Category = y[i].ToString(), Number = PY[i] });
            OnPropertyChanged("XTheoryValue");
            OnPropertyChanged("YTheoryValue");

            MX = numsX.Sum() / numsX.Count;
            MY = numsY.Sum() / numsY.Count;
            MXt = x.Zip(PX, (a, b) => a * b).Sum();
            MYt = y.Zip(PY, (a, b) => a * b).Sum();

            DX = numsX.Select(a=>Math.Pow(a, 2)).Sum()/ numsX.Count- Math.Pow(MX,2);
            DY = numsY.Select(a => Math.Pow(a, 2)).Sum()/numsY.Count- Math.Pow(MY,2);
            DXt = x.Zip(PX, (a, b) => Math.Pow(a, 2) * b).Sum() - Math.Pow(MXt, 2);
            DYt = y.Zip(PY, (a, b) => Math.Pow(a, 2) * b).Sum() - Math.Pow(MYt,2);

            R = ( (nums.Select(val => val.Item1 * val.Item2).Sum() / nums.Count()) - (MX * MY) ) / (Math.Sqrt(DX) * Math.Sqrt(DY));
            var tempRt = 0.0;
            for (int i = 0; i < x.GetLength(0); i++)
                for (int j = 0; j < y.GetLength(0); j++)
                    tempRt += x[i] * y[j] * P[i, j];
            Rt =( tempRt - (MXt * MYt) ) /( Math.Sqrt(DXt)* Math.Sqrt(DYt));

            OnPropertyChanged("MX");
            OnPropertyChanged("MY");
            OnPropertyChanged("MXt");
            OnPropertyChanged("MYt");
            OnPropertyChanged("DX");
            OnPropertyChanged("DY");
            OnPropertyChanged("DXt");
            OnPropertyChanged("DYt");
            OnPropertyChanged("R");
            OnPropertyChanged("Rt");
        }

        private bool ReadValues(string path)
        {
            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(Lab3Model));

                Lab3Model newValues;
                // десериализация
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    newValues = (Lab3Model)formatter.Deserialize(fs);
                }

                P = newValues.GetP();
                x = newValues.x;
                y = newValues.y;
            }
            catch(Exception e)
            {
                MessageBox.Show("Не удалось десериализовать данные", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return true;
        }

        public IDelegateCommand OpenFile { get; protected set; }

        private void ExecuteOpenFile(object param)
        {
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.Filter = "XML-файлы (*.XML)|*.XML" + "|Все файлы (*.*)|*.*";
            myDialog.CheckFileExists = true;
            myDialog.Multiselect = false;
            string filepath = null;
            if (myDialog.ShowDialog() == true)
            {
                filepath = myDialog.FileName;
            }
            switch (param as string)
            {
                case "SourceFile":
                    SourceFile = filepath;
                    break;
            }
        }

        private bool CanExecuteOpenFile(object param)
        {
            return true;
        }

        public Lab3ViewModel()
        {
            OpenFile = new DelegateCommand(ExecuteOpenFile, CanExecuteOpenFile);
            _count = 100;
        }
    }
}
