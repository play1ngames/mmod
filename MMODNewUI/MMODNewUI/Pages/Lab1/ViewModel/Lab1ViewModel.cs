﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using FirstFloor.ModernUI.Presentation;
using MMODNewUI.Pages.Lab1.Task;

namespace MMODNewUI.Pages.Lab1.ViewModel
{
    //todo: критерий независимости, количество интервалов автоматически
    public class Lab1ViewModel: NotifyPropertyChanged
    {
        #region fields
        public ObservableCollection<Lab1Model> ItemsA1 { get; private set; }
        public ObservableCollection<Lab1Model> ItemsA2 { get; private set; }
        public ObservableCollection<Lab1Model> ItemsA3 { get; private set; }
        public ObservableCollection<Lab1Model> ItemsB1 { get; private set; }
        public ObservableCollection<Lab1Model> ItemsB2 { get; private set; }
        public ObservableCollection<Lab1Model> ItemsB3 { get; private set; }

        public ObservableCollection<Lab1ModelGroup> GroupA { get; set; }
        public ObservableCollection<Lab1ModelGroup> GroupB { get; set; }
        #endregion

        #region labsGenerators
        private int _maxCount1 = 1000;
        private int _maxCount2 = 10000;
        private int _maxCount3 = 100000;

        public int MaxCount1
        {
            get { return _maxCount1; }
            set
            {
                if(value > 0 && value < MaxCount2 && value < MaxCount3)
                {
                    _maxCount1 = value;
                    OnPropertyChanged("MaxCount1");
                    //todo: regen
                }
            }
        }
        public int MaxCount2 { get { return _maxCount2; }
            set
            {
                if (value > 0 && value > MaxCount1 && value < MaxCount3)
                {
                    _maxCount1 = value;
                    OnPropertyChanged("MaxCount2");
                    //todo: regen
                }
            }
        }
        public int MaxCount3 { get { return _maxCount3; }
            set
            {
                if (value > 0 && value > MaxCount2 && value > MaxCount1)
                {
                    _maxCount1 = value;
                    OnPropertyChanged("MaxCount3");
                    //todo: regen
                }
            }
        }

        private const int _S = 5;

        private int sqGenStart;
        public int SqGenStart {
            get { return sqGenStart; }
            set
            {//todo: validation
                if (sqGenStart != value && value >= 10000000 && value <= 99999999)
                {
                    this.sqGenStart = value;
                    GenGroupA();
                    OnPropertyChanged("SqGenStart");
                }
            }
        }

        #region MandDandR1
        private decimal mA1;
        /// <summary>
        /// Мат ожидание первого графика первого метода
        /// </summary>
        public decimal MA1 { get; private set; }
        private decimal mA2;
        /// <summary>
        /// Мат ожидание второго графика первого метода
        /// </summary>
        public decimal MA2 { get; private set; }
        private decimal mA3;
        /// <summary>
        /// Мат ожидание третьего графика первого метода
        /// </summary>
        public decimal MA3 { get; private set; }
        private decimal dA1;
        /// <summary>
        /// Дисперсия первого графика первого метода
        /// </summary>
        public decimal DA1 { get; private set; }
        private decimal dA2;
        /// <summary>
        /// Дисперсия второго графика первого метода
        /// </summary>
        public decimal DA2 { get; private set; }
        private decimal dA3;
        /// <summary>
        /// Дисперсия третьего графика первого метода
        /// </summary>
        public decimal DA3 { get; private set; }

        private decimal rA1;
        /// <summary>
        /// Коэф корреляции первого графика первого метода
        /// </summary>
        public decimal RA1 { get; private set; }
        private decimal rA2;
        /// <summary>
        /// Коэф корреляции второго графика первого метода
        /// </summary>
        public decimal RA2 { get; private set; }
        private decimal rA3;
        /// <summary>
        /// Коэф корреляции третьего графика первого метода
        /// </summary>
        public decimal RA3 { get; private set; }

        private List<decimal> A;

        private void TestIndependanceA()
        {
            RA1 = RA2 = RA3 = 0;
            for(int i = 0; i < _maxCount1 - _S; i++)
            {
                RA1 += A[i] * A[i + _S];
            }
            for (int i = 0; i < _maxCount2 - _S; i++)
            {
                RA2 += A[i] * A[i + _S];
            }
            for (int i = 0; i < _maxCount3 - _S; i++)
            {
                RA3 += A[i] * A[i + _S];
            }
            RA1 = (RA1 * 12 / (_maxCount1 - _S)) - 3;
            RA2 = (RA2 * 12 / (_maxCount2 - _S)) - 3;
            RA3 = (RA3 * 12 / (_maxCount3 - _S)) - 3;
        }
        #endregion


        
        private void GenGroupA()
        {
            foreach (var x in ItemsA1)
                x.Count = 0;
            foreach (var x in ItemsA2)
                x.Count = 0;
            foreach (var x in ItemsA3)
                x.Count = 0;

            int count = 0;
            var sq = new SquareMethod(SqGenStart);
            MA1 = MA2 = MA3 = DA3 = DA2 = DA1 = 0;
            foreach(decimal v in sq.Sequence())
            {
                //decimal vv = v * 10;
                
                if (count == _maxCount3)
                    break;
                if (count <= _maxCount1)
                { 
                    ItemsA1[(int)Math.Truncate(v*ItemsA1.Count)].Count++;
                    MA1 += v;
                    DA1 += v * v;
                }
                if (count <= _maxCount2)
                {
                    ItemsA2[(int)Math.Truncate(v*ItemsA2.Count)].Count++;
                    MA2 += v;
                    DA2 += v * v;
                }
                ItemsA3[(int)Math.Truncate(v*ItemsA3.Count)].Count++;
                MA3 += v;
                DA3 += v * v;
                A.Add(v);
                count++;
            }
            MA1 /= _maxCount1;
            MA2 /= _maxCount2;
            MA3 /= _maxCount3;
            DA1 /= _maxCount1;
            DA2 /= _maxCount2;
            DA3 /= _maxCount3;
            DA1 -= MA1 * MA1;
            DA2 -= MA2 * MA2;
            DA3 -= MA3 * MA3;
            foreach (var x in ItemsA1)
                x.Count /= _maxCount1;
            foreach (var x in ItemsA2)
                x.Count /= _maxCount2;
            foreach (var x in ItemsA3)
                x.Count /= _maxCount3;
            TestIndependanceA();

        }

        private int multCongrGenStart;
        public int MultCongrGenStart
        {
            get { return multCongrGenStart; }
            set
            {
                if (multCongrGenStart != value && value >= 10000000 && value <= 99999999)
                {
                    this.multCongrGenStart = value;
                    GenGroupB();
                    OnPropertyChanged("ItemsB1");
                    OnPropertyChanged("ItemsB2");
                    OnPropertyChanged("ItemsB3");
                    OnPropertyChanged("GroupB");
                }
            }
        }

        #region MandDandR2
        private decimal mB1;
        /// <summary>
        /// Мат ожидание первого графика первого метода
        /// </summary>
        public decimal MB1 { get; private set; }
        private decimal mB2;
        /// <summary>
        /// Мат ожидание второго графика первого метода
        /// </summary>
        public decimal MB2 { get; private set; }
        private decimal mB3;
        /// <summary>
        /// Мат ожидание третьего графика первого метода
        /// </summary>
        public decimal MB3 { get; private set; }

        private decimal dB1;
        /// <summary>
        /// Дисперсия первого графика первого метода
        /// </summary>
        public decimal DB1 { get; private set; }
        private decimal dB2;
        /// <summary>
        /// Дисперсия второго графика первого метода
        /// </summary>
        public decimal DB2 { get; private set; }
        private decimal dB3;
        /// <summary>
        /// Дисперсия третьего графика первого метода
        /// </summary>
        public decimal DB3 { get; private set; }

        private decimal rB1;
        /// <summary>
        /// Коэф корреляции первого графика первого метода
        /// </summary>
        public decimal RB1 { get; private set; }
        private decimal rB2;
        /// <summary>
        /// Коэф корреляции второго графика первого метода
        /// </summary>
        public decimal RB2 { get; private set; }
        private decimal rB3;
        /// <summary>
        /// Коэф корреляции третьего графика первого метода
        /// </summary>
        public decimal RB3 { get; private set; }

        private List<decimal> B;

        private void TestIndependanceB()
        {
            RB1 = RB2 = RB3 = 0;
            for (int i = 0; i < _maxCount1 - _S; i++)
            {
                RB1 += B[i] * B[i + _S];
            }
            for (int i = 0; i < _maxCount2 - _S; i++)
            {
                RB2 += B[i] * B[i + _S];
            }
            for (int i = 0; i < _maxCount3 - _S; i++)
            {
                RB3 += B[i] * B[i + _S];
            }
            RB1 = (RB1 * 12 / (_maxCount1 - _S)) - 3;
            RB2 = (RB2 * 12 / (_maxCount2 - _S)) - 3;
            RB3 = (RB3 * 12 / (_maxCount3 - _S)) - 3;
        }
        #endregion

        private void GenGroupB()
        {
            foreach (var x in ItemsB1)
                x.Count = 0;
            foreach (var x in ItemsB2)
                x.Count = 0;
            foreach (var x in ItemsB3)
                x.Count = 0;

            MB1 = MB2 = MB3 = DB3 = DB2 = DB1 = 0;
            int count = 0;
            var sq = new MultiplicativeCongruentMethod(MultCongrGenStart);
            foreach (decimal v in sq.Sequence())
            {
                //decimal vv = v*10;
                if (count == _maxCount3)
                    break;
                if (count < _maxCount1)
                {
                    ItemsB1[(int)Math.Truncate(v*ItemsB1.Count)].Count++;
                    MB1 += v;
                    DB1 += v * v;
                }
                if (count < _maxCount2)
                {
                    ItemsB2[(int)Math.Truncate(v*ItemsB2.Count)].Count++;
                    MB2 += v;
                    DB2 += v * v;
                }
                ItemsB3[(int)Math.Truncate(v*ItemsB3.Count)].Count++;
                MB3 += v;
                DB3 += v * v;
                count++;
                B.Add(v);
            }
            MB1 /= _maxCount1;
            MB2 /= _maxCount2;
            MB3 /= _maxCount3;
            DB1 /= _maxCount1;
            DB2 /= _maxCount2;
            DB3 /= _maxCount3;
            DB1 -= MB1 * MB1;
            DB2 -= MB2 * MB2;
            DB3 -= MB3 * MB3;
            foreach (var x in ItemsB1)
                x.Count /= _maxCount1;
            foreach (var x in ItemsB2)
                x.Count /= _maxCount2;
            foreach (var x in ItemsB3)
                x.Count /= _maxCount3;
            TestIndependanceB();
        }

        #endregion

        //Количество интервалов в зависимости от количества элементов
        private int GetIntervalsCount(int n)
        {
            if (n <= 100)
                return (int)Math.Sqrt(n);
            return (int)(2*(int)Math.Round(Math.Log(n)));
        }


        public Lab1ViewModel()
        {
            A = new List<decimal>();
            B = new List<decimal>();

            ItemsA1 = new ObservableCollection<Lab1Model>();
            ItemsA2 = new ObservableCollection<Lab1Model>();
            ItemsA3 = new ObservableCollection<Lab1Model>();
            for (int i = 1; i <= GetIntervalsCount(_maxCount1); i++)
                ItemsA1.Add(new Lab1Model() { Category = i.ToString(), Count = 0 });
            for (int i = 1; i <= GetIntervalsCount(_maxCount2); i++)
                ItemsA2.Add(new Lab1Model() { Category = i.ToString(), Count = 0 });
            for (int i = 1; i <= GetIntervalsCount(_maxCount3); i++)
                ItemsA3.Add(new Lab1Model() { Category = i.ToString(), Count = 0 });

            GroupA = new ObservableCollection<Lab1ModelGroup>();
            GroupA.Add(new Lab1ModelGroup { DisplayName = _maxCount1.ToString(), Items = ItemsA1 });
            GroupA.Add(new Lab1ModelGroup { DisplayName = _maxCount2.ToString(), Items = ItemsA2 });
            GroupA.Add(new Lab1ModelGroup { DisplayName = _maxCount3.ToString(), Items = ItemsA3 });

            ItemsB1 = new ObservableCollection<Lab1Model>();
            ItemsB2 = new ObservableCollection<Lab1Model>();
            ItemsB3 = new ObservableCollection<Lab1Model>();
            for (int i = 1; i <= GetIntervalsCount(_maxCount1); i++)
                ItemsB1.Add(new Lab1Model() { Category = i.ToString(), Count = 0 });
            for (int i = 1; i <= GetIntervalsCount(_maxCount2); i++)
                ItemsB2.Add(new Lab1Model() { Category = i.ToString(), Count = 0 });
            for (int i = 1; i <= GetIntervalsCount(_maxCount3); i++)
                ItemsB3.Add(new Lab1Model() { Category = i.ToString(), Count = 0 });

            GroupB = new ObservableCollection<Lab1ModelGroup>();
            GroupB.Add(new Lab1ModelGroup { DisplayName = _maxCount1.ToString(), Items = ItemsB1 });
            GroupB.Add(new Lab1ModelGroup { DisplayName = _maxCount2.ToString(), Items = ItemsB2 });
            GroupB.Add(new Lab1ModelGroup { DisplayName = _maxCount3.ToString(), Items = ItemsB3 });

            // add the default themes
            this.themes.Add(new Link { DisplayName = "dark", Source = AppearanceManager.DarkThemeSource });
            this.themes.Add(new Link { DisplayName = "light", Source = AppearanceManager.LightThemeSource });
            AppearanceManager.Current.PropertyChanged += OnAppearanceManagerPropertyChanged;
            SyncTheme();

            SqGenStart = 12345678;
            MultCongrGenStart = 12345678;

        }

        #region themes
        private LinkCollection themes = new LinkCollection();
        private Link selectedTheme;
        public Link SelectedTheme
        {
            get { return this.selectedTheme; }
            set
            {
                if (this.selectedTheme != value)
                {
                    this.selectedTheme = value;
                    OnPropertyChanged("SelectedTheme");
                    OnPropertyChanged("DarkLayout");
                    OnPropertyChanged("Foreground");
                    OnPropertyChanged("Background");
                    OnPropertyChanged("MainBackground");
                    OnPropertyChanged("MainForeground");
                    // and update the actual theme
                    AppearanceManager.Current.ThemeSource = value.Source;
                }
            }
        }

        public string Foreground
        {
            get
            {
                if (selectedTheme.DisplayName.Equals("dark"))
                {
                    return "#FFEEEEEE";
                }
                return "#FF666666";
            }
        }
        public string MainForeground
        {
            get
            {
                if (selectedTheme.DisplayName.Equals("dark"))
                {
                    return "#FFFFFFFF";
                }
                return "#FF666666";
            }
        }
        public string Background
        {
            get
            {
                if (selectedTheme.DisplayName.Equals("dark"))
                {
                    return "#FF333333";
                }
                return "#FFF9F9F9";
            }
        }
        public string MainBackground
        {
            get
            {
                if (selectedTheme.DisplayName.Equals("dark"))
                {
                    return "#FF000000";
                }
                return "#FFEFEFEF";
            }
        }


        private void OnAppearanceManagerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ThemeSource")
            {
                SyncTheme();
            }
        }

        private void SyncTheme()
        {
            // synchronizes the selected viewmodel theme with the actual theme used by the appearance manager.
            this.SelectedTheme = this.themes.FirstOrDefault(l => l.Source.Equals(AppearanceManager.Current.ThemeSource));
        }


    #endregion

    }

}
