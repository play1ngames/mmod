﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMODNewUI.Pages.Lab1.ViewModel
{
    // class which represent a data point in the chart
    public class Lab1Model
    {
        public string Category { get; set; }

        public double Count { get; set; }
    }

    public class Lab1ModelGroup
    {
        public string DisplayName { get; set; }
        public ObservableCollection<Lab1Model> Items { get; set; }
    }

    
}
