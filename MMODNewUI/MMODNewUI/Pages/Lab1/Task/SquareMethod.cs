﻿using System;
using System.Collections.Generic;

namespace MMODNewUI.Pages.Lab1.Task
{
    /// <summary>
    /// Генератор БСВ(базовой случайной величины)
    /// Метод середины квадрата
    /// </summary>
    public class SquareMethod : IBaseRandomVariable
    {
        private int key;

        /// <summary>
        /// Базовый конструктор на 8 разрядов
        /// </summary>
        /// <param name="key"> key = [10 000 000 ; 99 999 999]</param>
        public SquareMethod(int key)
        {
            if (key < 10000000 || key > 99999999)
            {
                throw new ArgumentException("key must be in [10 000 000; 99 999 999]");
            }
            this.key = key;
        }

        /// <summary>
        /// Генератор псевдослучайных чисел
        /// </summary>
        /// <returns>Последовательность базовой случайной величины</returns>
        public IEnumerable<decimal> Sequence()
        {
            long prev = key;
            while (true)
            {
                //костыль на случай если первые цифры из середины будут 0
                while (prev < 10000000)
                    prev += 10000001;
                long next = prev * prev;
                if (next < 1000000000000000)
                    next *= 10;

                prev = Int64.Parse(next.ToString().Substring(4, 8));
                decimal res = prev;
                res /= 100000000;
                yield return res;
            }
        }

    }
}
