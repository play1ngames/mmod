﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMODNewUI.Pages.Lab1.Task
{
    /// <summary>
    /// Генератор БСВ(базовой случайной величины)
    /// Мультипликативный конгруэнтный метод
    /// </summary>
    public class MultiplicativeCongruentMethod: IBaseRandomVariable
    {
        private long A0;
        private long k;
        private long m;

        public MultiplicativeCongruentMethod(int key, long k = 123456789, long m = 4294967296) // m = 2^32
        {
            //todo: Check key
            this.A0 = key;
            this.k = k;
            this.m = m;
        }

        /// <summary>
        /// Генератор псевдослучайных чисел
        /// </summary>
        /// <returns>Последовательность базовой случайной величины</returns>
        public IEnumerable<decimal> Sequence()
        {
            decimal prev = A0;
            while (true)
            {
                decimal next = prev;
                next *= k;
                next %= m;
                prev = next;
                next /= m;
                yield return next;
            }
        }
    }
}
