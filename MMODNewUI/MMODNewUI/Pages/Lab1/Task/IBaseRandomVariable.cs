﻿
using System.Collections.Generic;

namespace MMODNewUI.Pages.Lab1.Task
{
    /// <summary>
    /// Интерфейс генератора БСВ(базовой случайной величины)
    /// </summary>
    public interface IBaseRandomVariable
    {
        IEnumerable<decimal> Sequence();
    }
}
