﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMODNewUI.Pages.Lab4.Task
{

    public abstract class Generator
    {
        protected static Random rnd = new Random(1);

        public abstract double GenerateNextSignalTime(double currentTime);
    }

    /// <summary>
    /// Генератор равномерного распределения
    /// </summary>
    public class LinearGenerator : Generator
    {
        double min;
        double max;

        public LinearGenerator(double min, double max)
        {
            this.min = min;
            this.max = max;
        }

        public override double GenerateNextSignalTime(double currentTime)
        {
            var t = min + rnd.NextDouble() * (max - min);
            return currentTime + t;
        }
    }

    /// <summary>
    /// Генератор экспоненциального распределения
    /// </summary>
    public class ExponentialGenerator : Generator
    {
        double mean;

        public ExponentialGenerator(double mean)
        {
            this.mean = mean;
        }

        public override double GenerateNextSignalTime(double currentTime)
        {
            var t = -mean * Math.Log(rnd.NextDouble());
            return currentTime + t;
        }
    }

    /// <summary>
    /// Генератор нормального распределения
    /// </summary>
    public class NormalGenerator: Generator
    {
        double mu;
        double sigma;

        public NormalGenerator(double mu, double sigma)
        {
            this.mu = mu;
            this.sigma = sigma;
        }

        public override double GenerateNextSignalTime(double currentTime)
        {
            double dSumm = 0;
            for (int i = 0; i <= 12; i++)
            {
                double R = rnd.NextDouble();
                dSumm = dSumm + R;
            }
            double t = Math.Round((mu + sigma * (dSumm - 6)), 3);
            return currentTime + t;
        }

    }
}
