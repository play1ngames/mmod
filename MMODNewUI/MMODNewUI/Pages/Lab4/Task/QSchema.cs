﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMODNewUI.Pages.Lab4.Task
{
    internal class QSchema
    {
        int[,] Z;//состояние канала
        double[,] T;//время окончания обслуживания???
        double TN;//текущее время???
        double TM; //заявочное время
        public int N1 { get; private set; } // количество потерянных заявок
        public int N3 { get; private set; }  //количество обслуженных заявок
        int[] ZN;//состояние накопителя 
        int[] L;//емкость накопителя

        double deltaT;
        int N;
        Generator U, K1, K2, K3;

        #region metrics
        /// <summary>
        /// веротность отклонения заявки
        /// </summary>
        public double P1 { get { return N1 / (N1 + N3); } }
        /// <summary>
        /// среднее время обслуживания заявки
        /// </summary>
        public double AvgT { get { return TN / N3; } }
        /// <summary>
        /// количество итераций
        /// </summary>
        public long ICount { get { return (long)(TN / deltaT); } }
        private long avgB1;
        /// <summary>
        /// средний размер буффера 1ой фазы
        /// </summary>
        public double AvgB1 { get { return avgB1/(TM/deltaT); } }
        private long avgB2;
        /// <summary>
        /// средний размер буффера 2ой фазы
        /// </summary>
        public double AvgB2 { get { return avgB2 / (TM / deltaT); } }

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="U">генератор св для источника заявок</param>
        /// <param name="K1">ген для каналов 1</param>
        /// <param name="K2"> ген для каналов 2</param>
        /// <param name="K3"> ген для каналов 3</param>
        /// <param name="deltaT">приращение времени</param>
        /// <param name="N">количество заявок</param>
        /// <param name="L">емкость накопителей</param>
        public QSchema(Generator U, Generator K1, Generator K2, Generator K3, double deltaT, int N, int[] L )
        {
            this.U = U;
            this.K1 = K1;
            this.K2 = K2;
            this.K3 = K3;

            this.deltaT = deltaT;
            this.N = N;
            this.L = L;

            Z = new int[3, 2];
            T = new double[3, 2];
            TN = TM = 0.0;
            N1 = N3 = 0;
            ZN = new int[2];
        }

        private void Step4()
        {
            if(Z[2,0] == 1) //4.1 //если занят обсдуживанием
            {
                if(T[2,0] <= TN) //4.2 //проверяем время
                {
                    N3 = N3 + 1;//4.3 ..+ к обслуженным
                    Z[2, 0] = 0;//4.4 канал свободен
                }
            }
        }

        private void Step5()
        {
            for(int j = 0; j <= 1; j++)//5.1 5.9 5.10
            {
                if(Z[1,j] != 0)//5.2 канал занят или заблочен
                {
                    if(T[1,j] <= TN)//5.3 // хранит ранее заблоченную
                    { //или именно в этот момент завершил обслуживание
                        if(Z[2,0] == 0)//5.4 //check канала 3ей фазы
                        {//если свободен
                            //WORK(T[2, 0]);//5.6//формируем время хверщ заявки
                            T[2, 0] = K3.GenerateNextSignalTime(TN);
                            Z[2, 0] = 1;//5.7
                            Z[1, j] = 0;//5.8
                        }
                        else
                        { //если занят
                            Z[1, j] = 2;//5.5
                        }
                    }
                }
            }
        }

        private void Step6()
        {
            for(int j =0; j <= 1; j++) //6.1 6.7 6.8
            {
                if(ZN[1]<=0) //6.2 проверка состояния накопителя
                {
                    break;
                }
                else
                {
                    if(Z[1, j] ==0)//6.3 //проверка состояния канала
                    {
                        //WORK(T[1, j]);//6.4 время обработки для канала
                        T[1, j] = K2.GenerateNextSignalTime(TN);
                        Z[1, j] = 1;//6.5 состояние канала
                        ZN[1] = ZN[1] - 1;//6.6 изменения объема занятого накопителя
                    }
                }
            }
        }

        private void Step7()
        {
            for(int j = 0; j <= 1; j++) //7.1 7.15 7.16
            {
                if(Z[0, j] != 0)//7.2
                {
                    if(T[0, j] <= TN)//7.3
                    {
                        bool tag = true;
                        for(int i = 0; i <= 1; i++)//7.4 7.6 7.7
                        {
                            if(Z[1,i] == 0)//7.5
                            {
                                //WORK(T[1, i]);//7.8
                                T[1, i] = K2.GenerateNextSignalTime(TN);
                                Z[1, i] = 1;//7.9
                                Z[0, j] = 0;//7.10
                                tag = false;
                                break;
                            }
                        }
                        if(tag)
                        {
                            if(ZN[1] < L[1])//7.11
                            {
                                ZN[1] = ZN[1] + 1;//7.12
                                Z[0, j] = 0;//7.14
                            }
                            else
                            {
                                Z[0, j] = 2;//7.13
                            }
                        }

                    }
                }
            }
        }

        private void Step8()
        {
            for(int j = 0; j <= 1; j++ )//8.1 8.7 8.8
            {
                if(ZN[0] > 0)//8.2
                {
                    if(Z[0,j]==0)//8.3
                    {
                        //WORK(T[0, j]);//8.4
                        T[0, j] = K1.GenerateNextSignalTime(TN);
                        Z[0, j] = 1;//8.5
                        ZN[0] = ZN[0] - 1;//8.6
                    }
                }
            }
        }

        private void Step9()
        {
            if (TM > TN) //9.1!!!
                return;
            bool tag = true;
            for(int j = 0; j <= 1; j++)//9.1 9.6 9.7
            {
                if(Z[0,j] == 0)//9.3
                {
                    //WORK(T[0, j]);//9.4
                    T[0, j] = K1.GenerateNextSignalTime(TN);
                    Z[0, j] = 1;//9.5
                    tag = false;
                    break;
                }
            }
            if(tag)
            {
                if (ZN[0] < L[0])//9.8
                {
                    ZN[0] = ZN[0] + 1;//9.9
                }
                else
                {
                    N1 = N1 + 1;//9.10
                }
            }
            //D(TM);//9.11
            TM = U.GenerateNextSignalTime(TN); //to do: какое время кидать в генератор?
        }

        /// <summary>
        /// переход к след моменту времени
        /// </summary>
        private void Step10()
        {
            TN += deltaT;
            avgB1 += ZN[0];
            avgB2 += ZN[1];
        }

        /// <summary>
        /// проверка окончания моделирования
        /// </summary>
        private void Step3()
        {
            while(N1 + N3 != N)
            {
                Step4();
                Step5();
                Step6();
                Step7();
                Step8();
                Step9();
                Step10();
            }
        }

        public void Solver(ref int N1, ref int N3)
        {
            Step3();
            N1 = this.N1;
            N3 = this.N3;
        }

    }
}
